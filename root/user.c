/**************************************************************************//**
 * @file user.c
 * @brief handle all data in user data segment
 * @version 1.0.0
 ******************************************************************************
 *
 ******************************************************************************/
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <intrinsics.h>
#include <em_msc.h>
#include "main.h"
#include "crc.h"
#include "user.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define USERDATA_BASE     (0x0FE00000UL) /**< User data page base address */
#define USER_PAGE_WORDS 128

#define USERDATA  ((uint32_t *) USERDATA_BASE)
#define SERIALNUM   0
#define SERIALCRC   1

/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
 
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief read serial number stored in flash if crc doesn't match rewrite serial
 *                            number.
 *   
 * @param[in] serialNum - default serial number 
 *
 * @return  serial number of device
 ******************************************************************************/

uint32_t userGetSerialNum(uint32_t serialNum)
{
  uint32_t storedSerNum = USERDATA[SERIALNUM];
  uint32_t storedCRC = USERDATA[SERIALCRC];
  if((crc32((uint8_t*)&storedSerNum,sizeof(storedSerNum)) != storedCRC)||
     (storedSerNum == 0xFFFFFFFFUL) ||(storedSerNum == 0))
       /* if no serial number stored*/
       /* write new serial number and crc */
  { 
    uint32_t userPageCopy[USER_PAGE_WORDS]; 
    for(int i = 0; i < USER_PAGE_WORDS; i++)userPageCopy[i] = USERDATA[i];
    userPageCopy[SERIALNUM] = serialNum;
    userPageCopy[SERIALCRC] = crc32((uint8_t*)&serialNum,sizeof(storedSerNum));
    MSC_ErasePage(USERDATA);
    // Rewrite user page with new data
    MSC_WriteWord(USERDATA, userPageCopy, sizeof(userPageCopy));
    storedSerNum = serialNum;   
  }
  return storedSerNum;
}
/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/