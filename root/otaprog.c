#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
//#include <em_leuart.h>
//#include <em_gpio.h>
#include <em_msc.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "main.h"
#include "radio.h"
#include "otaprog.h"
#include "rtc.h"
#include "check.h"
#include "crc.h"


/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

#define PAGE_SIZE 0x1000
#define MESS_SIZE 128


/*******************************************************************************
 ***************************   global variables*********************************
 ******************************************************************************/
uint8_t *progArr;
uint8_t *progArrEnd;
uint8_t *progArrStart;
uint32_t *pagesAddrArr;
uint32_t progLength, progCRC;
bool progArrError = false;
uint32_t startTimer = 0;
uint8_t otaprogData[10]; 
//extern receive_t RADIO_IN;
bool endProgMess = false; //end of receiving bin file flag
bool dataValid; //flag of validation received data for reprogramming


/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

bool fillMem(uint8_t *buffer, uint32_t startAddr, uint32_t length);
uint32_t align(uint32_t val, uint32_t aligner);

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/*******************************************************************************
 * @brief wait for reprogram command, 
 *                              then reprograms flash and reset system
 *   
 * @param[in] mode RESTART - write
 * @param[in] period - if return, setup rtc timer for sleep period
 * @param[in] RADIO_IN data from LoRa
 ******************************************************************************/
void otaprogRout(uint8_t mode,uint32_t period,receive_t *RADIO_IN,uint32_t SerialNumber)
{
   
      radioReadRXBuf(RADIO_IN); //check message
      if(checkProgram1(SerialNumber, RADIO_IN))
      { 
        otaprogData[0] = READY_PROG1_MESS;
        otaprogData[1] = SerialNumber;
        radioTest(otaprogData, 2, POWER, false ,txBand(SerialNumber));
        while(!getTXDoneFlag()); //wait for transmission to finish
        radioRXCon(PROG_BAND);      //wait for check reprogramm comand
        rtcWait(100);//wait 0.1 sec to answer and set RTC to ms
        startTimer = rtcGetMS() + 5000;  //wait 5 sec to receive PROGRAM2_MESS (0x0B)
        while(rtcGetMS() < startTimer)
        {
          if(getRXDoneFlag())
          {
            radioReadRXBuf(RADIO_IN); //check message
            if(checkProgram2(SerialNumber, RADIO_IN))
            {
              otaprogData[0] = READY_PROG2_MESS;
              otaprogData[1] = SerialNumber;
              otaprogData[2] = RADIO_IN->RSSI;
              otaprogData[3] = RADIO_IN->RSSI >> 8; 
              if(otaprogPrepare(RADIO_IN->buffer))
              { 
                otaprogData[4] = FILE_OK;  
                radioTest(otaprogData, 5, POWER, false ,txBand(SerialNumber));
                while(!getTXDoneFlag()); //wait for transmission to finish
                if(otaprogMalloc())   //memory allocation for bin file
                {
                  endProgMess = false;        //
                  radioRXCon(PROG_BAND);      //wait for data
                  startTimer = rtcGetMS() + 3000;
                  while(rtcGetMS() < startTimer)
                  {
                    if(getRXDoneFlag())
                    {
                      radioReadRXBuf(RADIO_IN); //read message
                      endProgMess = otaprogReadMess(RADIO_IN->buffer);
                      startTimer = rtcGetMS() + 1000; //wait next message no longer than 1 sec
                    }
                    if(endProgMess)
                    {
                      dataValid = otaprogValid();
                      otaprogData[0] = FILE_RECEIVED;
                      otaprogData[1] = SerialNumber;
                      otaprogData[2] = dataValid?REC_OK:REC_ERROR;
                      rtcWait(200);//wait 0.2 sec to answer after receiving last message
                      radioTest(otaprogData, 3, POWER, false ,txBand(SerialNumber));
                      while(!getTXDoneFlag()); //wait for transmission to finish
                      if(dataValid)otaprogProg(mode);  // no return from this function; // RESTART | NO_RESTART
                      break;
                    }
                  }
                  otaprogFree();   //memory free from bin file
                }//end_if(otaprogMalloc())
              }
              else
              {
                otaprogData[4] = FILE_TOO_BIG;  
                radioTest(otaprogData, 5, POWER, false ,txBand(SerialNumber));
                while(!getTXDoneFlag()); //wait for transmission to finish
              }
            }
            break;
          }//end_if(getRXDoneFlag()) 
        }//end_while
        rtcSetWakeUp(period);   //if returns from program that somethig went wrong 
                                // continue to work
      }
   
}
/*******************************************************************************
 * @brief received data for flash reprogramming
 * if data valid  then reprograms flash and reset system
 *   
 * @param[in] mode FOR_RESTART - write
 * @return    false - error true - ok
 ******************************************************************************/

bool otaprogProg (uint8_t mode)
{  
  
  if(mode != RESTART)return(!fillMem(progArrStart, 0x10000, ALIGN(progLength,4)));
  fillMem(progArrStart, 0, ALIGN(progLength,4)); //reprogram memory from 0 addr then reset
  
  return true; // no need it'll never return
}
/*******************************************************************************
 * @brief check bin file length and save CRC
 *   
 * @param[in] message - reference to received data
 * @return    false - memory allocation error
 *            true  - ok
 ******************************************************************************/
bool otaprogPrepare(uint8_t *message)
{
  progLength = message[2] + ((uint32_t)message[3] << 8);
  progCRC = message[4] \
            + ((uint32_t)message[5] << 8) \
            + ((uint32_t)message[6] << 16)\
            + ((uint32_t)message[7] << 24);
  if(!(progArrStart = (uint8_t *)malloc(progLength)))return false;
  //*progArr++ = 1;
  free(progArrStart); 
  //progArr = NULL;
  return true;
}
/*******************************************************************************
 * @brief memory allocate for bin file usin progLength
 *   
 * @param[in] none
 * @return    true - memory allocated
 *            false  - memory allocation error
 ******************************************************************************/
bool otaprogMalloc(void)
{
  if(!(progArrStart = (uint8_t *)malloc(progLength)))return false; //error
  
  progArrEnd = progArrStart + progLength; //find address of the last 
  /*use progArr pointer for incrimenting becaus if malloc() argument becames more
   than initial value + length then free() will call hardware error*/
  progArr = progArrStart;             //element of received bin file
  return true;
}
/*******************************************************************************
 * @brief free progArr memory
 * @param[in] none
 * @return    none
 ******************************************************************************/
void otaprogFree(void)
{
  if(progArrStart)free(progArrStart);
}
/*******************************************************************************
 * @brief write to progArr received message 
 *              
 *   
 * @param[in] buffer - 129 bytes first - counter 128- data
 * @return    true - end of data
 *            false - otherwise
 ******************************************************************************/
#define END_OF_DATA true
#define NO_END_OF_DATA false
bool otaprogReadMess(uint8_t *buffer)
{
  for(int i = 0; i < MESS_SIZE; i++)
  {
    *progArr = buffer[i+1];
    progArr++;
    if(progArr > progArrEnd) return END_OF_DATA; 
  } 
  return NO_END_OF_DATA;
}
/*******************************************************************************
 * @brief check crc and length of received bin file
 *   
 * @return    true - if ok
 *            false - otherwise
 ******************************************************************************/
uint32_t crcTemp;
bool otaprogValid(void)
{
  crcTemp = crc32(progArrStart, progLength);
  if(crcTemp == progCRC) return true;
  return false;
}
/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/

/**
  \brief   System Reset
  \details Initiates a system reset request to reset the MCU.
 */
__ramfunc void reset(void)
{
  __disable_interrupt();
   __DSB();                                                          /* Ensure all outstanding memory accesses included
                                                                       buffered write are completed before reset */
  SCB->AIRCR  = (uint32_t)((0x5FAUL << SCB_AIRCR_VECTKEY_Pos)    |
                           (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
                            SCB_AIRCR_SYSRESETREQ_Msk    );         /* Keep priority group unchanged */
  __DSB();                                                          /* Ensure completion of memory access */

  for(;;)                                                           /* wait until reset */
  {
    __NOP();
  }
}
/*******************************************************************************
 * @brief write region of flash memory
 *   
 * @param[in] buffer - array of data to write to the flash
 * @param[in] startAddr - where to write from
 * @param[in] length - number of bytes to write.
 * @return    fals - ok
 *            true - memory writing error
 ******************************************************************************/

uint32_t *addr;

__ramfunc bool ramFillMem(uint8_t *buffer, uint32_t startAddr, uint32_t length)
{
  uint32_t addrVal = 0;
  bool error = false;
    
  for(int i = 0; addrVal < startAddr + length;i++)
  {
    addrVal = startAddr + i*PAGE_SIZE;
    addr = (uint32_t*)addrVal;
    if(MSC_ErasePage(addr))
    {
      error = true;
      break;
    }
  }
  
  if(!error)
  {
    length += (length%4)? 4 - length%4 : 0;   
    if(MSC_WriteWord((uint32_t*)startAddr, buffer, length))error = true;
  }
  
  /*MSC_Deinit();*/ 
  MSC->WRITECTRL &= ~MSC_WRITECTRL_WREN; // Disable writing to the Flash.
  MSC->LOCK = 0; // Lock the MSC module.
  if(startAddr == 0) reset();
  return error;
}

bool fillMem(uint8_t *buffer, uint32_t startAddr, uint32_t length)
{
  bool error = false;
  __istate_t s = __get_interrupt_state();
  __disable_interrupt();
  MSC_Init();
  if(ramFillMem(buffer, startAddr, length))error = true;
  __set_interrupt_state(s);
  return error;
}

/*******************************************************************************
 * @brief increase data to aligner aligned
 *   
 * @param[in] val valume to be aligned
 * @param[in] aligner 
 * 
 * @return    data aligned to aligner
 ******************************************************************************/
uint32_t align(uint32_t val, uint32_t aligner) 
{
  //val = (val%aligner)?val:val + aligner - val%aligner;
  //#define ALIGN(v)(a) ((v%a)?v:v+a-v%a)
  if((val % aligner)==0)return val;
  return (val + aligner - val%aligner);
}