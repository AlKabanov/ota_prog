/**************************************************************************//**
 * @file act.c
 * @brief module for counting activity using accelerometer
 * @version 1.0.0
 ******************************************************************************
 *
 ******************************************************************************/
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
//#include "radio.h"
#include "main.h"
#include "act.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define DIVIDER 20
/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
 static bool startDiff = false;
 uint32_t diffX, diffY, diffZ;
 static int16_t prevX, prevY, prevZ;
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief calculating average and differencial
 *   
 * @param[in] x axe of accelerometer reference 
 * @param[in] y
 * @param[in] x
 * @param[in] samples = number of samples
 *
 * @return    
 ******************************************************************************/
   
void actAverage(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
  int32_t summX = 0, summY = 0, summZ = 0;
  for(int i = 0; i < samples; i++){summX += x[i], summY += y[i], summZ += z[i];}
  summX /=samples, summY /=samples, summZ /=samples;
  if(startDiff == false)
  {
    startDiff = true;
    prevX = summX, prevY = summY, prevZ = summZ;
    diffX = 0, diffY = 0, diffZ = 0;
  }
  diffX += ABS(summX-prevX), diffY += ABS(summY-prevY), diffZ += ABS(summZ-prevZ);
  prevX = summX, prevY = summY, prevZ = summZ;
}
/***************************************************************************//**
 * @brief sending current average acytivity divided by diveder to be uint8_t
 * @param [in] minutes - how many minutes period lasts
 * @return mean activity  per minute
 ******************************************************************************/
uint8_t actGetActivity(uint8_t minutes)
{
  uint32_t activity;
  activity = (diffX + diffY + diffX)/(3*minutes);  //average activity for 1 minute
  activity = (activity > (DIVIDER*0xFF))?0xff:(activity/DIVIDER);
  return (uint8_t)activity;
  
}
/***************************************************************************//**
 * @brief clear summs of activities
 *   
 ******************************************************************************/
void actResetActivity(void)
{
  diffX = 0, diffY = 0, diffZ = 0;
}