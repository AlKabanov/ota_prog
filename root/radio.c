#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include <em_usart.h>
#include <intrinsics.h>
#include "rtc.h"
#include "spi.h"
#include "gpio.h"
#include "radio.h"

// Registers Mapping
#define RegFifo                                    0x00 // common
#define RegOpMode                                  0x01 // common
#define FSKRegBitrateMsb                           0x02
#define FSKRegBitrateLsb                           0x03
#define FSKRegFdevMsb                              0x04
#define FSKRegFdevLsb                              0x05
#define RegFrfMsb                                  0x06 // common
#define RegFrfMid                                  0x07 // common
#define RegFrfLsb                                  0x08 // common
#define RegPaConfig                                0x09 // common
#define RegPaRamp                                  0x0A // common
#define RegOcp                                     0x0B // common
#define RegLna                                     0x0C // common
#define FSKRegRxConfig                             0x0D
#define LORARegFifoAddrPtr                         0x0D
#define FSKRegRssiConfig                           0x0E
#define LORARegFifoTxBaseAddr                      0x0E
#define FSKRegRssiCollision                        0x0F
#define LORARegFifoRxBaseAddr                      0x0F 
#define FSKRegRssiThresh                           0x10
#define LORARegFifoRxCurrentAddr                   0x10
#define FSKRegRssiValue                            0x11
#define LORARegIrqFlagsMask                        0x11 
#define FSKRegRxBw                                 0x12
#define LORARegIrqFlags                            0x12 
#define FSKRegAfcBw                                0x13
#define LORARegRxNbBytes                           0x13 
#define FSKRegOokPeak                              0x14
#define LORARegRxHeaderCntValueMsb                 0x14 
#define FSKRegOokFix                               0x15
#define LORARegRxHeaderCntValueLsb                 0x15 
#define FSKRegOokAvg                               0x16
#define LORARegRxPacketCntValueMsb                 0x16 
#define LORARegRxpacketCntValueLsb                 0x17 
#define LORARegModemStat                           0x18 
#define LORARegPktSnrValue                         0x19 
#define FSKRegAfcFei                               0x1A
#define LORARegPktRssiValue                        0x1A 
#define FSKRegAfcMsb                               0x1B
#define LORARegRssiValue                           0x1B 
#define FSKRegAfcLsb                               0x1C
#define LORARegHopChannel                          0x1C 
#define FSKRegFeiMsb                               0x1D
#define LORARegModemConfig1                        0x1D 
#define FSKRegFeiLsb                               0x1E
#define LORARegModemConfig2                        0x1E 
#define FSKRegPreambleDetect                       0x1F
#define LORARegSymbTimeoutLsb                      0x1F 
#define FSKRegRxTimeout1                           0x20
#define LORARegPreambleMsb                         0x20 
#define FSKRegRxTimeout2                           0x21
#define LORARegPreambleLsb                         0x21 
#define FSKRegRxTimeout3                           0x22
#define LORARegPayloadLength                       0x22 
#define FSKRegRxDelay                              0x23
#define LORARegPayloadMaxLength                    0x23 
#define FSKRegOsc                                  0x24
#define LORARegHopPeriod                           0x24 
#define FSKRegPreambleMsb                          0x25
#define LORARegFifoRxByteAddr                      0x25
#define LORARegModemConfig3                        0x26
#define FSKRegPreambleLsb                          0x26
#define FSKRegSyncConfig                           0x27
#define LORARegFeiMsb                              0x28
#define FSKRegSyncValue1                           0x28
#define LORAFeiMib                                 0x29
#define FSKRegSyncValue2                           0x29
#define LORARegFeiLsb                              0x2A
#define FSKRegSyncValue3                           0x2A
#define FSKRegSyncValue4                           0x2B
#define LORARegRssiWideband                        0x2C
#define FSKRegSyncValue5                           0x2C
#define FSKRegSyncValue6                           0x2D
#define FSKRegSyncValue7                           0x2E
#define FSKRegSyncValue8                           0x2F
#define FSKRegPacketConfig1                        0x30
#define FSKRegPacketConfig2                        0x31
#define LORARegDetectOptimize                      0x31
#define FSKRegPayloadLength                        0x32
#define FSKRegNodeAdrs                             0x33
#define LORARegInvertIQ                            0x33
#define FSKRegBroadcastAdrs                        0x34
#define FSKRegFifoThresh                           0x35
#define FSKRegSeqConfig1                           0x36
#define FSKRegSeqConfig2                           0x37
#define LORARegDetectionThreshold                  0x37
#define FSKRegTimerResol                           0x38
#define FSKRegTimer1Coef                           0x39
#define LORARegSyncWord                            0x39
#define FSKRegTimer2Coef                           0x3A
#define FSKRegImageCal                             0x3B
#define FSKRegTemp                                 0x3C
#define FSKRegLowBat                               0x3D
#define FSKRegIrqFlags1                            0x3E
#define FSKRegIrqFlags2                            0x3F
#define RegDioMapping1                             0x40 // common
#define RegDioMapping2                             0x41 // common
#define RegVersion                                 0x42 // common
#define FSKRegPllHop                               0x44
#define RegTcxo                                    0x4B // common
#define RegPaDac                                   0x4d // common
#define RegFormerTemp                              0x5B // common
#define FSKRegBitRateFrac                          0x5D 
#define RgAgcRef                                   0x61 //common
#define RegAgcThresh1                              0x62 // common
#define RegAgcThresh2                              0x63 // common
#define RegAgcThresh3                              0x64 // common
#define RegPll                                     0x70 // common

#define LORA_MAX_BUF_LEN        140
#define TCXO                    0x10
#define LNA_RX_GAIN (0x20|0x3)

// sx1276 RegModemConfig1
#define MC1_BW_125                0x70
#define MC1_BW_250                0x80

typedef enum{
  bw_125 = MC1_BW_125,
  bw_250 = MC1_BW_250,
}bw_t;

#define MC1_CR_4_5            0x02
#define MC1_CR_4_6            0x04
#define MC1_CR_4_7            0x06
#define MC1_CR_4_8            0x08

#define MC1_IMPLICIT_HEADER_MODE_ON    0x01 
// sx1276 RegModemConfig2   
#define MC2_SF7                 0x70
#define MC2_SF8                 0x80
#define MC2_SF9                 0x90
#define MC2_SF10                0xA0
#define MC2_SF11                0xB0
#define MC2_SF12                0xC0
#define MC2_RX_PAYLOAD_CRCON        0x04

typedef enum{
  sf7 = MC2_SF7,
  sf8 = MC2_SF8,
  sf9 = MC2_SF9,
  sf10 = MC2_SF10,
  sf11 = MC2_SF11,
  sf12 = MC2_SF12,
}sf_t;

// sx1276 RegModemConfig3          
#define MC3_LOW_DATA_RATE_OPTIMIZE  0x08
#define MC3_AGCAUTO                 0x04

// preamble for lora networks (nibbles swapped)
#define LORA_PREAMBLE                  0x34
#define SYNC_WORD                      0x34//0x92//0x34//

#define RXLORA_RXMODE_RSSI_REG_MODEM_CONFIG1 0x0A
#define RXLORA_RXMODE_RSSI_REG_MODEM_CONFIG2 0x70

// DIO function mappings                D0D1D2D3
#define MAP_DIO0_LORA_RXDONE   0x00  // 00------
#define MAP_DIO0_LORA_TXDONE   0x40  // 01------
#define MAP_DIO1_LORA_RXTOUT   0x00  // --00----
#define MAP_DIO1_LORA_NOP      0x30  // --11----
#define MAP_DIO2_LORA_NOP      0xC0  // ----11--

#define MAP_DIO0_FSK_READY     0x00  // 00------ (packet sent / payload ready)
#define MAP_DIO1_FSK_NOP       0x30  // --11----
#define MAP_DIO2_FSK_TXNOP     0x04  // ----01--
#define MAP_DIO2_FSK_TIMEOUT   0x08  // ----10--


// ---------------------------------------- 
// Constants for radio registers
#define OPMODE_LORA      0x80
#define OPMODE_MASK      0x07
#define OPMODE_SLEEP     0x00
#define OPMODE_STANDBY   0x01
#define OPMODE_FSTX      0x02
#define OPMODE_TX        0x03
#define OPMODE_FSRX      0x04
#define OPMODE_RX        0x05
#define OPMODE_RX_SINGLE 0x06 
#define OPMODE_CAD       0x07 

typedef enum
{
  lora = OPMODE_LORA,
  sleep = OPMODE_SLEEP,
  standby = OPMODE_STANDBY,
  fstx = OPMODE_FSTX,
  tx = OPMODE_TX,
  fsrx = OPMODE_FSRX,
  rx = OPMODE_RX,
  rx_single = OPMODE_RX_SINGLE,
  cad = OPMODE_CAD,
}opmode_t;

// ----------------------------------------
// Bits masking the corresponding IRQs from the radio
#define IRQ_LORA_RXTOUT_MASK 0x80
#define IRQ_LORA_RXDONE_MASK 0x40
#define IRQ_LORA_CRCERR_MASK 0x20
#define IRQ_LORA_HEADER_MASK 0x10
#define IRQ_LORA_TXDONE_MASK 0x08
#define IRQ_LORA_CDDONE_MASK 0x04
#define IRQ_LORA_FHSSCH_MASK 0x02
#define IRQ_LORA_CDDETD_MASK 0x01

// FSK IMAGECAL defines
#define RF_IMAGECAL_AUTOIMAGECAL_MASK               0x7F
#define RF_IMAGECAL_AUTOIMAGECAL_ON                 0x80
#define RF_IMAGECAL_AUTOIMAGECAL_OFF                0x00  // Default

#define RF_IMAGECAL_IMAGECAL_MASK                   0xBF
#define RF_IMAGECAL_IMAGECAL_START                  0x40

#define RF_IMAGECAL_IMAGECAL_RUNNING                0x20
#define RF_IMAGECAL_IMAGECAL_DONE                   0x00  // Default

#define ONE_FREQ_TRANS 

#ifdef ONE_FREQ_TRANS 
#define BAND     band0
#endif




static void writeReg (uint8_t addr, uint8_t data ) {
    gpioNSS(0);
    spiSend(addr | 0x80);
    spiSend(data);
    gpioNSS(1);
}

static uint8_t readReg (uint8_t addr) {
    gpioNSS(0);
    spiSend(addr & 0x7F);
    uint8_t val = spiSend(0x00);
    gpioNSS(1);
    return val;
}

static void writeBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
    gpioNSS (0);
    spiSend(addr | 0x80);
    if(len > LORA_MAX_BUF_LEN)len = LORA_MAX_BUF_LEN;
    for (uint8_t i=0; i<len; i++) {
        spiSend(buf[i]);
    }
    gpioNSS(1);
}

static void readBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
    gpioNSS (0);
    spiSend(addr & 0x7F);
    if(len > LORA_MAX_BUF_LEN)len = LORA_MAX_BUF_LEN;
    for (uint8_t i=0; i<len; i++) {
        buf[i] = spiSend(0x00);
    }
    gpioNSS(1);
}


static void opmode (opmode_t mode) {
    writeReg(RegOpMode, (readReg(RegOpMode) & ~OPMODE_MASK) | mode);
}

void radioLoraConfig(bw_t bw, sf_t sf, band_t band)
{
  uint8_t mc1 =0, mc2 = 0, mc3 =0;
   
  writeReg(RegOpMode,sleep);
  opmode(lora);
  
  writeReg(RegFrfMsb, (uint8_t)(band>>16));
  writeReg(RegFrfMid, (uint8_t)(band>> 8));
  writeReg(RegFrfLsb, (uint8_t)(band>> 0));
  
  mc1 = bw|MC1_CR_4_5;//|MC1_IMPLICIT_HEADER_MODE_ON;
  mc2 = readReg(LORARegModemConfig2)&0x03;      //preserve SymbTimeout(9:8)
  mc2 |= sf|MC2_RX_PAYLOAD_CRCON;
  if((sf == sf11)||(sf == sf12)) mc3 = MC3_LOW_DATA_RATE_OPTIMIZE;
  mc3 |= MC3_AGCAUTO;
  
  writeReg(LORARegModemConfig1,mc1);
  writeReg(LORARegModemConfig2,mc2);
  writeReg(LORARegModemConfig3,mc3);
  writeReg(LORARegSyncWord,SYNC_WORD);
  writeReg(RegTcxo,(readReg(RegTcxo))&(~TCXO));  //using Crystal
   
}

static void radioSetPower(uint8_t pw)
{
  if(pw > 14)pw = 14;
  uint8_t power = 0xf0|pw;
  writeReg(RegPaConfig,power);
}



#ifdef ONE_FREQ_TRANS 
band_t bandArr[] = {BAND,BAND,BAND,BAND,BAND,BAND,BAND,BAND};   // all messages are sending om oe frequency
#else
band_t bandArr[] = {band0,band1,band2,band3,band4,band5,band6,band7};
#endif


bool TXDoneFlag = false;
bool RXDoneFlag = false;
uint8_t regVal = 0, regVal0, regVal1, regVal2;
uint8_t bandLen = sizeof(bandArr)/sizeof(band_t);
uint8_t iterator = 0;
uint8_t bandCounter = 0;
uint8_t TXCounter = 0;
uint32_t startTime = 0, stopTime = 0, difTime = 0;

/************************************************************************
interrupt callback functions
************************************************************************/
static void radioRXDONEInterrupt(void)
{
  RXDoneFlag = true;
  regVal = readReg( RegOpMode);  //debug
}

static void radioTXDONEInterrupt(void)
{
  TXDoneFlag = true;
  regVal1 = readReg( RegOpMode);  //debug
  stopTime = rtcGetMS();
  //difTime = stopTime - startTime;
  //opmode(standby);
   // set bitrate
  //  regVal0 = readReg(RegOpMode); 
   __no_operation();  // regVal0 == standby;
   TXCounter++;
}

#define POLY 0x4C11DB7

uint16_t crc16(uint8_t *p, uint8_t len)
{
  uint16_t x;
  uint16_t crc = 0xFFFF;
   
  for(uint8_t i = 0; i < len; i++)
  {
    x = crc >> 8 ^ p[i];
    x ^= x>>4;
    crc = (crc << 8) ^ (x << 12) ^ (x <<5) ^ x;
  }
  return crc;
}

uint16_t check;

/* test sx1276 without transmittion
   return  0 - test failed  1 - test passed */
uint8_t deb1,deb2,deb3,deb4,deb5,deb6,deb7,deb8;  //when test lora should be 0f 07 d8 06 09 4f f9 81
 
uint16_t radioTest(uint8_t *data, uint8_t dataLen, uint8_t power, bool crc16On,band_t band)
{
  
  
  //uint8_t dataLen = sizeof(data);
  //data[0] = iterator++;
  
  //gpioLDOOn();
  if(crc16On)
  {
    uint16_t crc = crc16(data, dataLen-2);   
    data[dataLen - 2] = crc >> 8;
    data[dataLen - 1] = crc;
    
    check = crc16(data, dataLen);   
  }
  deb1 = readReg( RegOpMode);
  
  writeReg(RegOpMode,0);
  
  deb2 = readReg( RegOpMode);
  //radioSetFreq(band0);
  radioLoraConfig(bw_125,sf8,band);
  //radioLoraConfig(bw_250,sf8,band);
  
  if(++bandCounter == bandLen)bandCounter = 0;
  
 deb3 = readReg( RegFrfMsb);
 deb4 = readReg( RegFrfMid);
 deb5 = readReg(RegTcxo);
  
 deb6 = readReg( RegPaConfig); //debug
  radioSetPower(power);
 deb7 = readReg( RegPaConfig); //debug
  
  writeReg(RegPaRamp, (readReg(RegPaRamp) & 0xF0) | 0x08); // set PA ramp-up time 50 uSec
  
  // set the IRQ mapping DIO0=TxDone DIO1=NOP DIO2=NOP
  writeReg(RegDioMapping1, MAP_DIO0_LORA_TXDONE|MAP_DIO1_LORA_NOP|MAP_DIO2_LORA_NOP);
  
  // set callback fo interrupt from DIO0  
  GPIO_SetCallback(rfDIO0, radioTXDONEInterrupt, RF_DIO0_MASK);
  
  // clear all radio IRQ flags
  writeReg(LORARegIrqFlags, 0xFF);
  // mask all IRQs but TxDone
  writeReg(LORARegIrqFlagsMask, ~IRQ_LORA_TXDONE_MASK);
  
  opmode(standby);
  
  deb8 = readReg( RegOpMode); //debug
  
  // initialize the payload size and address pointers    
  writeReg(LORARegFifoTxBaseAddr, 0x00);
  writeReg(LORARegFifoAddrPtr, 0x00);
  writeReg(LORARegPayloadLength, dataLen);
     
  // download buffer to the radio FIFO
  writeBuf(RegFifo, data, dataLen);

  
  // now we actually start the transmission
  opmode(tx);
  return 1;
  
}
/*************************************************************************
 init FSK
**************************************************************************/
void radioFSKInit(void)
{
  opmode(sleep);
  // some sanity checks, e.g., read version number
  regVal0 = readReg(RegVersion); 
  __no_operation();      //regVal0 == 0x12;
  
  // chain calibration
    writeReg(RegPaConfig, 0);
  
  // Sets a Frequency in HF band
    uint32_t frf = 868000000;
    writeReg(RegFrfMsb, (uint8_t)(frf>>16));
    writeReg(RegFrfMid, (uint8_t)(frf>> 8));
    writeReg(RegFrfLsb, (uint8_t)(frf>> 0));

    // Launch Rx chain calibration for HF band 
    writeReg(FSKRegImageCal, (readReg(FSKRegImageCal) & RF_IMAGECAL_IMAGECAL_MASK)|RF_IMAGECAL_IMAGECAL_START);
    while((readReg(FSKRegImageCal) & RF_IMAGECAL_IMAGECAL_RUNNING) == RF_IMAGECAL_IMAGECAL_RUNNING) { ; }

    opmode(sleep);
}
/*************************************************************************
 send data in FSK mode
*************************************************************************/
void radioFSKSend(uint8_t *data, uint8_t dataLen,uint8_t power)
{
   regVal0 = readReg(RegOpMode);
   regVal0 &= OPMODE_MASK;
   __no_operation();  // regVal0 == 0;
   // select FSK modem (from sleep mode)
   writeReg(RegOpMode, 0x10); // FSK, BT=0.5
   regVal0 = readReg(RegOpMode); 
   __no_operation();  // regVal0 == 0x10;
    // enter standby mode (required for FIFO loading))
   opmode(standby);
   // set bitrate
    regVal0 = readReg(RegOpMode); 
   __no_operation();  // regVal0 == standby;
   writeReg(FSKRegBitrateMsb, 0x02); // 50kbps
   writeReg(FSKRegBitrateLsb, 0x80);
    // set frequency deviation
   writeReg(FSKRegFdevMsb, 0x01); // +/- 25kHz
   writeReg(FSKRegFdevLsb, 0x99);
    // frame and packet handler settings
   writeReg(FSKRegPreambleMsb, 0x00);
   writeReg(FSKRegPreambleLsb, 0x05);
   writeReg(FSKRegSyncConfig, 0x12);
   writeReg(FSKRegPacketConfig1, 0xD0);
   writeReg(FSKRegPacketConfig2, 0x40);
   writeReg(FSKRegSyncValue1, 0xC1);
   writeReg(FSKRegSyncValue2, 0x94);
   writeReg(FSKRegSyncValue3, 0xC1);
    // configure frequency
   writeReg(RegFrfMsb, (uint8_t)(BAND>>16));
   writeReg(RegFrfMid, (uint8_t)(BAND>> 8));
   writeReg(RegFrfLsb, (uint8_t)(BAND>> 0));
    // configure output power
   radioSetPower(power);
    // set the IRQ mapping DIO0=PacketSent DIO1=NOP DIO2=NOP
   writeReg(RegDioMapping1, MAP_DIO0_FSK_READY|MAP_DIO1_FSK_NOP|MAP_DIO2_FSK_TXNOP);
    
    // set callback fo interrupt from DIO0  
  GPIO_SetCallback(rfDIO0, radioTXDONEInterrupt, RF_DIO0_MASK);
    // initialize the payload size and address pointers    
   writeReg(FSKRegPayloadLength, dataLen+1); // (insert length byte into payload))
    
    // download length byte and buffer to the radio FIFO
   writeReg(RegFifo, dataLen);
   writeBuf(RegFifo, data, dataLen);
    
    // now we actually start the transmission
   opmode(tx);
   regVal0 = readReg(RegOpMode); 
   __no_operation();  // regVal0 == 0x10;
    regVal0 = readReg(RegOpMode); 
   __no_operation();  // regVal0 == 0x10;
 
}


void nextTx(uint8_t *data, uint8_t dataLen, bool crc16On)
{
  startTime = rtcGetMS();
  if(crc16On)
  {
    uint16_t crc = crc16(data, dataLen-2);   
    data[dataLen - 2] = crc >> 8;
    data[dataLen - 1] = crc;
    
    check = crc16(data, dataLen);   
  }
  
  
  // clear all radio IRQ flags
  writeReg(LORARegIrqFlags, 0xFF);
  // mask all IRQs but TxDone
  writeReg(LORARegIrqFlagsMask, ~IRQ_LORA_TXDONE_MASK);
  
  opmode(standby);
  // initialize the payload size and address pointers    
  writeReg(LORARegFifoTxBaseAddr, 0x00);
  writeReg(LORARegFifoAddrPtr, 0x00);
  writeReg(LORARegPayloadLength, dataLen);
     
  // download buffer to the radio FIFO
  writeBuf(RegFifo, data, dataLen);

  
  // now we actually start the transmission
  opmode(tx);
}

void radioSleep(void)
{
  writeReg(RegOpMode,sleep);
  opmode(lora);
  regVal = readReg( RegOpMode); //debug
  opmode(sleep); 
  regVal = readReg( RegOpMode);  //debug
}

void radioRXCon(band_t band)
{
  regVal = readReg( RegOpMode);
  writeReg(RegOpMode, 0x00); 
  regVal = readReg( RegOpMode);
  
  //radioSetFreq(band0);
  if(band == bandLoraCom) radioLoraConfig(bw_250,sf7,band);
  else radioLoraConfig(bw_125,sf8,band); 
  
  //radioLoraConfig(bw_250,sf9,BAND);
  // set LNA gain
  writeReg(RegLna, LNA_RX_GAIN); 
  // set max payload size
  writeReg(LORARegPayloadMaxLength, LORA_MAX_BUF_LEN);
  // configure DIO mapping DIO0=RxDone DIO1=NOP DIO2=NOP
  writeReg(RegDioMapping1, MAP_DIO0_LORA_RXDONE|MAP_DIO1_LORA_NOP|MAP_DIO2_LORA_NOP);
  // set callback fo interrupt from DIO0  
  GPIO_SetCallback(rfDIO0, radioRXDONEInterrupt, RF_DIO0_MASK);
  // clear all radio IRQ flags
  writeReg(LORARegIrqFlags, 0xFF);
  // mask all IRQs but RxDone
  writeReg(LORARegIrqFlagsMask, ~IRQ_LORA_RXDONE_MASK);
  // continous rx (scan or rssi)
  opmode(rx); 
     
}

/*******************************************************

********************************************************/
uint8_t radioReadRXBuf(receive_t *data)
{
  RXDoneFlag = false;
  data->length = readReg(LORARegRxNbBytes);
  // set FIFO read address pointer
  writeReg(LORARegFifoAddrPtr, readReg(LORARegFifoRxCurrentAddr)); 
  // now read the FIFO
  readBuf(RegFifo, data->buffer, data->length);
  data->SNR = readReg(LORARegPktSnrValue); // SNR [dB] * 4
  if(data->SNR & 0x0080U)data->SNR |= 0xFF00U;
  data->SNR /=4;
  data->RSSI = readReg(LORARegPktRssiValue) - 157; // RSSI [dBm] (-196...+63)
  data->IRQ = readReg(LORARegIrqFlags);

  // mask all IRQs but RxDone
  writeReg(LORARegIrqFlagsMask, ~IRQ_LORA_RXDONE_MASK);
  // clear radio IRQ flags
  writeReg(LORARegIrqFlags, 0xFF);

  if(crc16(data->buffer, data->length)==0)
  {
    data->period = data->buffer[data->length - 3]; 
    data->dataLength = data->length - 3;  // 1byte - period 2,3 - crc
    data->dataValid = 1;
    return 1;
  }
  else
  {
    data->dataValid = 0;
    return 0;
  }
}


bool getRXDoneFlag(void)
{
  return RXDoneFlag;
}
bool getTXDoneFlag(void)
{
  if(TXDoneFlag == true)
  {
    TXDoneFlag = false;
    return true;
  }
  return false;
}