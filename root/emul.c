#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "main.h"
#include "radio.h"
#include "emul.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define PULSE_NO_CONNECTION 0
#define PULSE_FAILURE 1
#define PULSE_NO_MEASURE 2
#define COND_QUIET 0
#define COND_CHEW  1
#define COND_RUM   2
#define COND_NO    3
//#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
//#define BYTELOW(v)  (*((unsigned char *) (&v)))
//#define ACTIVITY_PARTS 4

typedef struct{
  uint32_t address;
  uint32_t counter;
  uint8_t  type;
  uint8_t  cowCond;
  uint8_t  cowActivity[ACTIVITY_PARTS];
  uint8_t  inT;
  uint8_t  outTLow;
  uint8_t  outTHigh;
  uint8_t  temp3;
  uint8_t  temp4;
  uint8_t  pulse;
  uint8_t  aes128[12];
} message_t;



/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
message_t message;
static uint32_t sensorAddress;
static uint8_t inT;
static uint16_t outT;
static uint8_t activityArr[ACTIVITY_PARTS];
static uint8_t cowCond = 0x00;
//static uint8_t messageLen = sizeof(message);
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/*******************************************************************************
 * @brief address setter
 *   
 * @param[in] address - address of sensor
 *  
 ******************************************************************************/
void emulSetAddr (uint32_t address)
{
  sensorAddress = address;
}
/*******************************************************************************
 * @brief inner temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetInT (uint8_t temperature)
{
  inT = temperature;
}
/*******************************************************************************
 * @brief outer temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetOutT (uint16_t temperature)
{
  outT = temperature;
}

/*******************************************************************************
 * @brief activity setter
 *   
 * @param[in] activity - activity value
 * @param[in] index - part of hour 1 - first 15 minutes, 4 - last 15 minutes
 ******************************************************************************/
void emulSetActivity (uint8_t activity, uint8_t index)
{
  if ((index > 0)&&(index < (ACTIVITY_PARTS+1)))
  {
    activityArr[index-1] = activity; 
  }
}



/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] counter - messages counter
 *  
 * @param[in] data - reference to array for message
 *
 * @param[in] dataLen - length of array
 *
 * @return   true if time to send message
 *   
 ******************************************************************************/
void emulMessage(uint32_t counter,uint8_t* data, uint8_t dataLen)
{
 
    message.address = sensorAddress;
    message.counter = counter;
    message.type = 0;
    message.cowCond = cowCond;  
    for(int i = 0; i < ACTIVITY_PARTS; i++)message.cowActivity[i] = activityArr[i];
    message.inT = inT;
    message.outTHigh = BYTEHIGH(outT);
    message.outTLow = BYTELOW(outT);
    message.temp3 = 0;
    message.temp4 = 0;
    message.pulse = PULSE_FAILURE; 
    for(int i = 0; i < dataLen;i++)data[i] = (*(((uint8_t *) (&message) + i)));  //send bytes from struct to array

}
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
uint8_t activityTestArr [] ={5,10,20,30,40,50,60,70,80,90,100,150,100,80,60,40,20,10};
void emulSetActivityTest(void)
{
  static uint8_t activityIndex = 0;
  if(++activityIndex == sizeof(activityTestArr))activityIndex = 0;
  activityArr[0] = activityTestArr[activityIndex];
  
}
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
uint8_t cowCondTestArr[] = {COND_QUIET, COND_CHEW, COND_RUM, COND_RUM, COND_CHEW, COND_CHEW, COND_NO, COND_NO,COND_QUIET,COND_CHEW};
void emulSetCondTest(void)
{
  static uint8_t cowCondIndex = 0;
  if(++cowCondIndex == sizeof(cowCondTestArr))cowCondIndex = 0;
  cowCond = cowCondTestArr[cowCondIndex];
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/

