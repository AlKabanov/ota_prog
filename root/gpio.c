
#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include "gpio.h"




/** Array of GPIO IRQ hooks for system/user/odd/even IRQs */
GPIO_hook_t GPIO_Hooks[hookSize];

/**************************************************************************//**
 * @brief Setup GPIO interrupt to set the time
 *****************************************************************************/
void gpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  GPIO_PinModeSet(PORT_SPI_CS, PIN_SPI_CS, gpioModePushPull, 1); // SX1276
  
  GPIO_PinModeSet(PORT_SPI_X_CS, PIN_SPI_X_CS, gpioModePushPull, 1); // memory
  
 //  Configure PC14 as input 
  GPIO_PinModeSet(RF_DIO0_PORT, RF_DIO0_PIN, gpioModeInput, 1);
  //  
  GPIO_PinModeSet(X_INT_PORT, X_INT_PIN, gpioModeInput, 1);

  // Set rasing edge interrupt 
  GPIO_IntConfig(RF_DIO0_PORT, RF_DIO0_PIN, true, false, true);
  // Set rasing edge interrupt 
  GPIO_IntConfig(X_INT_PORT, X_INT_PIN, true, false, true);
  
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
  
  
/*
  // Configure PB9 as input 
  GPIO_PinModeSet(gpioPortB, 9, gpioModeInput, 0);
  

  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 13, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
*/
  I2C_PWR_EN;
  TEMP_PWR_EN;
}


void gpioTEMPPWRON(void)
{
  TEMP_PWR_ON;
}

void gpioTEMPPWROFF(void)
{
  TEMP_PWR_OFF;
}
void gpioI2CPWRON(void)
{
  I2C_PWR_ON;
}

void gpioI2CPWROFF(void)
{
  I2C_PWR_OFF;
}



// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val)
{
  if(val == 0 || val == 1)
  { // drive pin
       GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModePushPull, 0);
       if (val)GPIO_PinOutSet(RST_PORT, RST_PIN);
       else    GPIO_PinOutClear(RST_PORT, RST_PIN);
  } else 
    { // keep pin floating
        GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModeDisabled, 1);
    }
}

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val)
{
	//not used 
}

// set radio NSS pin to given value
void gpioNSS (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Enable (Active Low)
		
}


// set axcelerometer NSS pin to given value
void gpioNSS_X (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_X_CS, PIN_SPI_X_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_X_CS, PIN_SPI_X_CS);	//  SPI Enable (Active Low)
		
}


/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB9)
 *        Sets the hours
 *****************************************************************************/

bool sleepFlag = false;
/*
void GPIO_ODD_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << 13); 
  sleepFlag = true;
}
*/
bool gpioGetSleepFlag(void)
{
  bool temp = sleepFlag;
  sleepFlag = false;
  return temp;
}



/**************************************************************************//**
 * @brief GPIO Interrupt handler 
 *****************************************************************************/
#define GPIO_EVEN_MASK	0x55555555						///< Mask for even interrupts



void GPIO_EVEN_IRQHandler(void)
{  
  uint32_t mask;
  // Get interrupt bits
  mask = GPIO_IntGet() & GPIO_EVEN_MASK; 
  // Acknowledge interrupt 
  GPIO_IntClear(mask);
  for(int i = 0; i < hookSize; i++)
  {
    if(GPIO_Hooks[i].callback && (mask & GPIO_Hooks[i].mask))
      GPIO_Hooks[i].callback();
  }
  
}

#define GPIO_ODD_MASK	0xAAAAAAAA						///< Mask for even interrupts



void GPIO_ODD_IRQHandler(void)
{  
  uint32_t mask;
  // Get interrupt bits
  mask = GPIO_IntGet() & GPIO_ODD_MASK; 
  // Acknowledge interrupt 
  GPIO_IntClear(mask);
  for(int i = 0; i < hookSize; i++)
  {
    if(GPIO_Hooks[i].callback && (mask & GPIO_Hooks[i].mask))
      GPIO_Hooks[i].callback();
  }
  
}