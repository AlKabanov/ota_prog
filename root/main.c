/**************************************************************************//**
 * @file
 * @brief firmware for necksensor for finding rumination detection algorithm
 * @version 1.0.1
 ******************************************************************************
 *
 ******************************************************************************/


#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"
//#include "mc3635SPI.h"

#include "main.h"
#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"
//#include "mc3635.h"
#include "i2c.h"
#include "check.h"
#include "linear.h"
#include "emul.h"
#include "act.h"
#include "otaprog.h"
#include "user.h"

//#define LORA_TEST 
//#define MC3635_TEST
//#define TEST_AZURE

//#define FAST
#define NORMAL
#define PERIOD_IN_MINUTES 10
#define CORRECTION 328

#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)
   

   

#ifdef FAST
      #define PERIOD_MIN 10
      #define MINUTE (ONE_SEC - 1)
#endif
#ifdef NORMAL
      #define PERIOD_MIN PERIOD_IN_MINUTES
      #define MINUTE ((ONE_SEC*1*60 - 1) - CORRECTION)
#endif






uint8_t data[32];               // 2 - crc
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
readStruct_t fromMC3635;        //, tempFromMC3635;
uint32_t sendCounter = 0, minCounter = 0;
uint32_t hourCounter = 0, time;
uint32_t serialNumber;



/*******************************************************************************
private function prototypes
*******************************************************************************/
void sleepMode(void);
void tests(void);

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
uint8_t *debArr;
void clockLoop(void)
{
 
    
//  radioRXCon(STANDBY_BAND);
  emulSetAddr(serialNumber);
  rtcSetWakeUp(MINUTE);  //sleep for 1 min
  time = rtcGetMS();
  radioSleep();
  
  while (1)
  {
     if(i2cGetFIFOintFlag() == true)
     {
        i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
        actAverage(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES);
     }
     if(time < rtcGetMS())  //one minute passed
     {  
       hourCounter++;
       if(hourCounter == PERIOD_MIN)
       {
         hourCounter = 0;
#ifdef TEST_AZURE
         emulSetActivityTest();
         emulSetCondTest();
#else
         emulSetActivity(actGetActivity(PERIOD_MIN),1);
#endif
         actResetActivity();
         //NVIC_EnableIRQ(GPIO_ODD_IRQn);
         //NVIC_EnableIRQ(GPIO_EVEN_IRQn);
         
         sendCounter++;
         
         emulMessage(sendCounter, data, sizeof(data));  //form message to send to server
         
         radioTest(data, sizeof(data), POWER, false, txBand(serialNumber));
         while(!getTXDoneFlag());          //wait for transmission to finish
         radioRXCon(STANDBY_BAND);      //wait for reprogramm command 
         for(int i = 600000; i > 0 ; i--);//wait 200ms 
         if(getRXDoneFlag())otaprogRout(NO_RESTART, MINUTE, &RADIO_IN,serialNumber);//reprogramming       
         radioSleep();
         //for(int i = 15000; i > 0 ; i--);//wait some time to sleep;
         
       }   
       time = rtcGetMS();
     }
     sleepMode();   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();

  /* Setup RTC to generate an interrupt */
  rtcSetup();  
  
  serialNumber = userGetSerialNum(ADDRESS);

  /* Setup GPIO*/
  gpioSetup();
  
  spiSetup();
  gpioRST(0);
  //gpioSetLED();
  rtcWait(200);
  //gpioClearLED();
  gpioRST(1);
  gpioI2CPWRON();
//  adcInit();
#ifndef LORA_TEST
  i2cMC3635Init();
#endif
  //MC3635SPIInit();
  //uartInit(9600, true);
  tests();
  /* Main function loop */
  clockLoop();

  return 0;
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/
void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  //NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  EMU_EnterEM2(true);
  
}

/*******************************************************************************
 * @brief some tests
 *   
 ******************************************************************************/
void tests(void)
{
    //gpioLDOOn();
#ifdef LORA_TEST
  data[0] = READY_MESS;
  data[1] = TX_ADDR;
  while(1)
  {
    test = radioTest(data, 4,POWER,false,band0);
    rtcWait(2000);
  }
  
#endif
#ifdef MC3635_TEST
  while(1)
  {
    if(i2cGetFIFOintFlag() == true)
    {
      i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
      
    }
  }
#endif

}
