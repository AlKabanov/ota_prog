
typedef struct {
	uint8_t period;
        uint8_t dataPtr;         //pointer to the buffer element last sent to usart
        uint8_t lastSent;        //byte sent to usart last 
        uint8_t dataValid;
        uint8_t dataLength;
        uint8_t IRQ;
	int16_t SNR;
        int16_t RSSI;
        uint8_t length;
        uint8_t buffer[255];
} receive_t;


typedef enum 
{
  band0 = (uint32_t)((864100UL << 11)/125),  //864.1MHz
  band1 = (uint32_t)((864300UL << 11)/125),  //864.3MHz
  band2 = (uint32_t)((864500UL << 11)/125),
  band3 = (uint32_t)((864640UL << 11)/125),
  band4 = (uint32_t)((864780UL << 11)/125),
  band5 = (uint32_t)((868780UL << 11)/125),
  band6 = (uint32_t)((868950UL << 11)/125),
  band7 = (uint32_t)((869120UL << 11)/125),
  bandLoraCom = (uint32_t)(869400UL << 11)/125, // 869.4MHz
  bandFSK = (uint32_t)(869200UL << 11)/125,
} band_t;

uint16_t radioTest(uint8_t *data, uint8_t dataLen,uint8_t power,bool crc16On, band_t band);

void nextTx(uint8_t *data, uint8_t dataLen, bool crc16On);
/*************************************************************************
 send data in FSK mode
*************************************************************************/
void radioFSKSend(uint8_t *data, uint8_t dataLen,uint8_t power);

void radioSleep(void);

/*************************************************************************
 init FSK
**************************************************************************/
void radioFSKInit(void);

/***************************
receive in continuous mode
***************************/
void radioRXCon(band_t band);
bool getRXDoneFlag(void);
bool getTXDoneFlag(void);
/**************************
read received data in receive continuous mode
**************************/
uint8_t radioReadRXBuf(receive_t *data);
