#ifndef MC3635_H
#define MC3635_H

#define MC3635_REG_EXT_STAT_1       (0x00)
#define MC3635_REG_EXT_STAT_2       (0x01)
#define MC3635_REG_XOUT_LSB         (0x02)
#define MC3635_REG_XOUT_MSB         (0x03)
#define MC3635_REG_YOUT_LSB         (0x04)
#define MC3635_REG_YOUT_MSB         (0x05)
#define MC3635_REG_ZOUT_LSB         (0x06)
#define MC3635_REG_ZOUT_MSB         (0x07)
#define MC3635_REG_STATUS_1         (0x08)
#define MC3635_REG_STATUS_2         (0x09)
#define MC3635_REG_MODE_C           (0x10)
#define MC3635_REG_WAKE_C           (0x11)
#define MC3635_REG_SNIFF_C          (0x12)
#define MC3635_REG_SNIFFTH_C        (0x13)
#define MC3635_REG_IO_C             (0x14)
#define MC3635_REG_RANGE_C          (0x15)

#define MC3635_REG_FIFO_C           (0x16)
#define MC3635_REG_INTR_C           (0x17)
#define MC3635_REG_PROD             (0x18)
#define MC3635_REG_POWER_MODE       (0x1C)
#define MC3635_REG_DMX              (0x20)
#define MC3635_REG_DMY              (0x21)

#define MC3635_REG_DMZ              (0x22)
#define MC3635_REG_RESET            (0x24)
#define MC3635_REG_XOFFL            (0x2A)
#define MC3635_REG_XOFFH            (0x2B)
#define MC3635_REG_YOFFL            (0x2C)
#define MC3635_REG_YOFFH            (0x2D)
#define MC3635_REG_ZOFFL            (0x2E)
#define MC3635_REG_ZOFFH            (0x2F)
#define MC3635_REG_XGAIN            (0x30)
#define MC3635_REG_YGAIN            (0x31)
#define MC3635_REG_ZGAIN            (0x32)
#define MC3635_REG_OPT              (0x3B)
#define MC3635_REG_LOC_X            (0x3C)
#define MC3635_REG_LOC_Y            (0x3D)
#define MC3635_REG_LOT_dAOFSZ       (0x3E)
#define MC3635_REG_WAF_LOT          (0x3F)

typedef enum
{
    RANGE_2G   = 0,
    RANGE_4G   = 1,
    RANGE_8G   = 2,
    RANGE_16G  = 3,
    RANGE_12G  = 4,	
}   MC3635_range_t;

typedef enum
{
    RESOLUTION_6BIT    = 0, 
    RESOLUTION_7BIT    = 1, 
    RESOLUTION_8BIT    = 2, 
    RESOLUTION_10BIT   = 3, 
    RESOLUTION_12BIT   = 4, 
    RESOLUTION_14BIT   = 5,  //(Do not select if FIFO enabled)
}   MC3635_resolution_t;


typedef enum
{
  DEFAULT = 0,
  LP_14_PRE_14                  = 0x05,
  ULP_25_LP_28_PRE_28           = 0x06,
  ULP_50_LP54_PRE_55            = 0x07,
  ULP_100_LP105_PRE_80          = 0x08,
  ULP_190_LP_210                = 0x09,
  ULP_380_LP_400                = 0x0A,
  ULP_750_LP_600                = 0x0B,
  ULP_1100                      = 0x0C,
  ULP_1300                      = 0x0F,
}   MC3635_cwake_ODR_t;

    
typedef enum
{
  SLEEP      = 0x0,
  STANDBY    = 0x01,
  SNIFF      = 0x02,
  CWAKE      = 0x05, 
  TRIG       = 0x07,  
} MC3635_mode_t;

typedef enum 
{
  LOW_POWER =0,
  ULOW_POWER = 3,
  PRE_POWER = 4,
}MC3635_power_t;

#endif /* MC3635_H */