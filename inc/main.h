#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************//**
 * @addtogroup MAIN
 * @{
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>

#define PAGE_SAMPLES 28
#define CHEW_MARK    5
  
#define ACTIVITY_PARTS 4

#define WAKE_MESS 1
#define READY_MESS 2
#define START_MESS 3
#define STOP_MESS 4
#define GO2STANDBY_MESS 5
#define ISALIVE_MESS 6
#define ALIVE_MESS 7
#define PROGRAM1_MESS 8
#define READY_PROG1_MESS 9
#define PROGRAM2_MESS 0xA  
#define READY_PROG2_MESS 0xB
#define FILE_RECEIVED 0x0C
   
#define FILE_OK 0
#define FILE_TOO_BIG 1
#define REC_OK 0
#define REC_ERROR 1
#define NO_RESTART 0
#define RESTART 1
   
#define ADDRESS 4
#define STANDBY_BAND band7
#define PROG_BAND bandLoraCom
#define POWER 10
   
#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
#define BYTELOW(v)  (*((unsigned char *) (&v)))
#define ABS(v)	   (((v) < 0) ? -(v) : (v))
#define ALIGN(v,a) ((v%a)?v:v - v%a + a)

typedef struct{
  int16_t xBuf[PAGE_SAMPLES];
  int16_t yBuf[PAGE_SAMPLES];
  int16_t zBuf[PAGE_SAMPLES];
  uint8_t sessionNum;
  uint8_t chewing[PAGE_SAMPLES/CHEW_MARK];
} readStruct_t;


typedef union{
  readStruct_t readStruct;
  uint8_t writeBuf[0x100];
} write2Mem_t;

typedef enum 
{
  standby,   //waiting for swiching on
  wait4tx,
  txRum,
  txNormal,
} mode_t;

/** @} (end addtogroup MAIN) */

#ifdef __cplusplus
}
#endif

#endif /* MAIN_H */
