/***************************************************************************//**
 * @file otaprog.h
 * @brief reprogramming uc over the air
 * @version 0.0.1
 *******************************************************************************
 ******************************************************************************/
 
#ifndef OTAPROG_H
#define OTAPROG_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************//**
 * @addtogroup OTAPROG
 * @{
 ******************************************************************************/
 /*******************************************************************************
 *******************************   STRUCTS   ***********************************
 ******************************************************************************/
 
 /*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
 
 /*******************************************************************************
 *****************************   PROTOTYPES   **********************************
 ******************************************************************************/

/*******************************************************************************
 * @brief received data for flash reprogramming
 * if data valid  then reprograms flash and reset system
 *   
 * @param[in] mode FOR_RESTART - write
 * @return    false - error true - ok
 ******************************************************************************/

bool otaprogProg (uint8_t mode);
/*******************************************************************************
 * @brief check bin file length and save CRC
 *   
 * @param[in] message - reference to received data
 * @return    false - memory allocation error
 *            true  - ok
 ******************************************************************************/
bool otaprogPrepare(uint8_t *message);
/*******************************************************************************
 * @brief memory allocate for bin file usin progLength
 *   
 * @param[in] none
 * @return    true - memory allocation error
 *            false  - memory allocated
 ******************************************************************************/
bool otaprogMalloc(void);
/*******************************************************************************
 * @brief free progArr memory
 * @param[in] none
 * @return    none
 ******************************************************************************/
void otaprogFree(void);
/*******************************************************************************
 * @brief write to progArr any received message check data integrity
 *              set progArrError if needed (CRC error,...)
 *   
 * @param[in] buffer - 129 bytes first - counter 128- data
 * @return    true - end of data
 *            false - otherwise
 ******************************************************************************/
bool otaprogReadMess(uint8_t *buffer);
/*******************************************************************************
 * @brief check crc and length of received bin file
 *   
 * @return    true - if ok
 *            false - otherwise
 ******************************************************************************/
bool otaprogValid(void);

/*******************************************************************************
 * @brief wait for reprogram command, 
 *                              then reprograms flash and reset system
 *   
 * @param[in] mode RESTART - write
 * @param[in] period - if return, setup rtc timer for sleep period
 * @param[in] RADIO_IN data from LoRa
 ******************************************************************************/
void otaprogRout(uint8_t mode,uint32_t period,receive_t *RADIO_IN,uint32_t SerialNumber);
 
/** @} (end addtogroup OTAPROG) */

#ifdef __cplusplus
}
#endif

#endif /* OTAPROG_H */